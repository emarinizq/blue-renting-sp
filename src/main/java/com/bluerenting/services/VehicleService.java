package com.bluerenting.services;

import com.bluerenting.models.VehicleModel;
import com.bluerenting.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    VehicleRepository vehicleRepository;

    public List<VehicleModel> findAll(){
        return this.vehicleRepository.findAll();
    }

    public Optional<VehicleModel> findById(String id){
        return this.vehicleRepository.findById(id);
    }

    public VehicleModel save(VehicleModel vehicle){
        return this.vehicleRepository.save(vehicle);
    }

    public void delete(String id){
        this.vehicleRepository.deleteById(id);
    }
}
