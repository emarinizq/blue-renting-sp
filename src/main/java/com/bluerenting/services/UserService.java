package com.bluerenting.services;

import com.bluerenting.models.UserModel;
import com.bluerenting.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(Optional<String> order){
        Sort sort = order.isPresent() && order.get().equals("asc") ? Sort.by("age").ascending() : Sort.by("age").descending();
        System.out.println("sort: " + sort.toString());
        return this.userRepository.findAll(sort);
    }

    public Optional<UserModel> findById(String id){
        return this.userRepository.findById(id);
    }

    public UserModel save(UserModel user){
        return this.userRepository.save(user);
    }

    public void delete(String id){
        this.userRepository.deleteById(id);
    }
}
