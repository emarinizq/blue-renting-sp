package com.bluerenting.services;

import com.bluerenting.models.PurchaseModel;
import com.bluerenting.models.VehicleModel;
import com.bluerenting.repositories.PurchaseRepository;
import com.bluerenting.repositories.UserRepository;
import com.bluerenting.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    VehicleRepository vehicleRepository;


    public List<PurchaseModel> findAll (Optional<String> userId, Optional<String> order){
        System.out.println("servicio findAll");
        
        Sort sort = Sort.by("date").descending();;
        if (order.isPresent() && order.get().equals("asc")){
            sort = Sort.by("date").ascending();
        }

        System.out.println("sort: " + sort.toString());
        if(userId.isPresent()){
            System.out.println("userId: " + userId.get());
            return this.purchaseRepository.findByUserId(userId.get(), sort);
        }else{
            return this.purchaseRepository.findAll(sort);
        }

    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("servicio findById:" + id);
        return this.purchaseRepository.findById(id);
    }

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase");
        PurchaseServiceResponse result = new PurchaseServiceResponse();

        if (this.userRepository.findById(purchase.getUserId()).isEmpty() == true) {
            System.out.println("El usuario de la compra no se ha encontrado");
            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        float totalPrice = 0;

        for (String idVehicles : purchase.getIdVehicles()) {
            if (this.vehicleRepository.findById(idVehicles).isEmpty()) {
                System.out.println("El producto con la id " + idVehicles + " no existe");
                result.setMsg("El producto con la id " + idVehicles + " no existe");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                VehicleModel vehicle = this.vehicleRepository.findById(idVehicles).get();
                System.out.println("Añadiendo valor del " + vehicle.getName());
                totalPrice += vehicle.getPrice();
            }
        }

        purchase.setTotalPrice(totalPrice);
        purchase = this.purchaseRepository.save(purchase);
        result.setPurchase(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        return result;
    }
}

