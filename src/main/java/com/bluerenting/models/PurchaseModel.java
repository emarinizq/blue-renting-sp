package com.bluerenting.models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "purchases")
public class PurchaseModel {
    @Id
    private String id ;
    private String userId;
    private float totalPrice;
    private List<String> idVehicles;
    @CreatedDate
    private Date date;

    public PurchaseModel() {
        this.date = new Date();
    }

    public PurchaseModel(String userId, float totalPrice, List<String> idVehicles) {
        this.userId = userId;
        this.totalPrice = totalPrice;
        this.idVehicles = idVehicles;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<String> getIdVehicles() {
        return idVehicles;
    }

    public void setIdVehicles(List<String> idVehicles) {
        this.idVehicles = idVehicles;
    }
}
