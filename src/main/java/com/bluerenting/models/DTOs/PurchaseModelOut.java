package com.bluerenting.models.DTOs;

import com.bluerenting.models.VehicleModel;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;
import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PurchaseModelOut {

    private String id ;
    private String userId;
    private float totalPrice;
    private List<VehicleModel> vehicles;
    private Date date;

    public PurchaseModelOut(String id, String userId, float totalPrice, List<VehicleModel> vehicles, Date date) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
        this.vehicles = vehicles;
        this.date = date;
    }

}
