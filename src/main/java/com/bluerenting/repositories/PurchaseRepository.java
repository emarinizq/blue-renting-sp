package com.bluerenting.repositories;

import com.bluerenting.models.PurchaseModel;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {

    List<PurchaseModel> findByUserId(String userId, Sort sort);

}
