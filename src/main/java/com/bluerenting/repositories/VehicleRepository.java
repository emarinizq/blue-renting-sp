package com.bluerenting.repositories;

import com.bluerenting.models.VehicleModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends MongoRepository<VehicleModel, String>{


}
