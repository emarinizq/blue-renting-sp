package com.bluerenting.controllers;

import com.bluerenting.models.UserModel;
import com.bluerenting.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/users")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class UserCtrl {

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam Optional<String> order){
        System.out.println("getUsers");
        return new ResponseEntity<>(this.userService.findAll(order), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel newUser){

        System.out.println("createUsers");

        return new ResponseEntity<>(this.userService.save(newUser), HttpStatus.CREATED );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){

        System.out.println("getUsers");

        Optional<UserModel> retrievedUser = this.userService.findById(id);

        return new ResponseEntity<>(
                retrievedUser.isPresent() ? retrievedUser.get() : "Usuario no encontrado",
                retrievedUser.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable String id, @RequestBody UserModel updateUser){

        System.out.println("updateUsers");

        Optional<UserModel> response = this.userService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND );
        }

        UserModel retrievedUser = response.get();


        if(updateUser.getName() != null){
            retrievedUser.setName(updateUser.getName());
        }

        if(updateUser.getAge() > 0){
            retrievedUser.setAge(updateUser.getAge());
        }

        return new ResponseEntity<>(this.userService.save(retrievedUser), HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable String id){

        System.out.println("deleteUsers");

        Optional<UserModel> response = this.userService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND );
        }else{
            this.userService.delete(id);
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        }

    }

}
