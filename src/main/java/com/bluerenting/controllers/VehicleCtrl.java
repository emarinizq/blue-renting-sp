package com.bluerenting.controllers;

import com.bluerenting.models.VehicleModel;
import com.bluerenting.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/vehicles")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class VehicleCtrl {

    @Autowired
    VehicleService vehicleService;

    @GetMapping
    public ResponseEntity<List<VehicleModel>> getVehicles(){

        System.out.println("getVehicles");

        return new ResponseEntity<>(this.vehicleService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<VehicleModel> addVehicle(@RequestBody VehicleModel newVehicle){

        System.out.println("createVehicles");

        return new ResponseEntity<>(this.vehicleService.save(newVehicle), HttpStatus.CREATED );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getVehicleById(@PathVariable String id){

        System.out.println("getVehicles");

        Optional<VehicleModel> retrievedVehicle = this.vehicleService.findById(id);

        return new ResponseEntity<>(
            retrievedVehicle.isPresent() ? retrievedVehicle.get() : "Vehicleo no encontrado",
            retrievedVehicle.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateVehicle(@PathVariable String id, @RequestBody VehicleModel updateVehicle){

        System.out.println("createVehicles");

        Optional<VehicleModel> response = this.vehicleService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Vehicleo no encontrado", HttpStatus.NOT_FOUND );
        }

        VehicleModel retrievedVehicle = response.get();


        if(updateVehicle.getName() != null){
            retrievedVehicle.setName(updateVehicle.getName());
        }

        if(updateVehicle.getModel() != null){
            retrievedVehicle.setModel(updateVehicle.getModel());
        }

        if(updateVehicle.getImageUrl() != null){
            retrievedVehicle.setImageUrl(updateVehicle.getImageUrl());
        }

        if(updateVehicle.getPrice() > 0){
            retrievedVehicle.setPrice(updateVehicle.getPrice());
        }

        return new ResponseEntity<>(this.vehicleService.save(retrievedVehicle), HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteVehicle(@PathVariable String id){

        System.out.println("createVehicles");

        Optional<VehicleModel> response = this.vehicleService.findById(id);

        if(!response.isPresent()){
            return new ResponseEntity<>("Vehicleo no encontrado", HttpStatus.NOT_FOUND );
        }else{
            this.vehicleService.delete(id);
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        }

    }

}
