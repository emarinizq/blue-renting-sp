package com.bluerenting.controllers;

import com.bluerenting.models.DTOs.PurchaseModelOut;
import com.bluerenting.models.PurchaseModel;
import com.bluerenting.models.VehicleModel;
import com.bluerenting.services.PurchaseService;
import com.bluerenting.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.bluerenting.services.PurchaseServiceResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/purchases")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST})

public class PurchaseCtrl {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    VehicleService vehicleService;

    @GetMapping
    public ResponseEntity<List<PurchaseModelOut>> getPurchases(@RequestParam Optional<String> order, @RequestParam Optional<String> userId){
        System.out.println("getPurchases");

        List<PurchaseModel> purchases = this.purchaseService.findAll(userId, order);

        List<PurchaseModelOut> outPurchases = new ArrayList<>();
        for(PurchaseModel purchase : purchases){

            List<VehicleModel> vehicles = new ArrayList<>();

            for(String vehicleId : purchase.getIdVehicles()) {
                Optional<VehicleModel> optionalVehicle = this.vehicleService.findById(vehicleId);

                if (optionalVehicle.isPresent()) {
                    vehicles.add(optionalVehicle.get());
                }
            }

            PurchaseModelOut outPurchase = new PurchaseModelOut(purchase.getId(), purchase.getUserId(), purchase.getTotalPrice(), vehicles, purchase.getDate());

            outPurchases.add(outPurchase);
        }


        return new ResponseEntity<>(outPurchases, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id){

        System.out.println("getPurchases");

        Optional<PurchaseModel> retrievedOptionalPurchase = this.purchaseService.findById(id);

        if(!retrievedOptionalPurchase.isPresent()){

            return new ResponseEntity<>("Pedido no encontrado",HttpStatus.NOT_FOUND );

        }else {

            PurchaseModel retrievedPurchaseModel = retrievedOptionalPurchase.get();

            List<VehicleModel> vehicles = new ArrayList<>();

            for (String vehicleId : retrievedPurchaseModel.getIdVehicles()) {

                Optional<VehicleModel> optionalVehicle = this.vehicleService.findById(vehicleId);

                if (optionalVehicle.isPresent()) {
                    vehicles.add(optionalVehicle.get());
                }
            }

            PurchaseModelOut outPurchase = new PurchaseModelOut(
                    retrievedPurchaseModel.getId(),
                    retrievedPurchaseModel.getUserId(),
                    retrievedPurchaseModel.getTotalPrice(),
                    vehicles,
                    retrievedPurchaseModel.getDate()
            );


            return new ResponseEntity<>(outPurchase, HttpStatus.OK);
        }
    }


    @PostMapping
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id del usuario de la compra es " + purchase.getUserId());
        System.out.println("Los coches de la compra son " + purchase.getIdVehicles());
        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
}

