package com.bluerenting.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloCtrl {

    @RequestMapping("/")
    public ResponseEntity<String> index(){
        return new ResponseEntity<>( "Hello World!!", HttpStatus.OK);
    }


    @RequestMapping("/hello")
    public String hello(@RequestParam(value="name", defaultValue = "Tech U V2") String name){
        return String.format("Hello %s", name);
    }

}